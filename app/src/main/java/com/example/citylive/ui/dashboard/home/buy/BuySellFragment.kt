package com.example.citylive.ui.dashboard.home.buy

import com.example.citylive.R
import com.example.citylive.base.BaseFragment
import com.example.citylive.databinding.FragmentBuySellBinding
import com.example.citylive.ui.dashboard.home.job.JobDetailActivity

import com.example.citylive.ui.dashboard.home.job.adapter.BuyAdapter
import com.example.citylive.util.openActivity

class BuySellFragment : BaseFragment<FragmentBuySellBinding>(FragmentBuySellBinding::class.java) {

    private val topPickAdapter by lazy { BuyAdapter(this::onItemClick) }
    override fun setViews() {


        binding.listTopPick.adapter = topPickAdapter
        topPickAdapter.submitList(arrayListOf(R.drawable.buy, R.drawable.buy2, R.drawable.buy3))
    }

    private fun onItemClick() {
//        val trans = activity?.supportFragmentManager?.beginTransaction()
//        trans?.add(R.id.dashboardContainer, TopOfferDetailFragment().apply { Bundle().apply { putParcelable(Intentkey.HOME_TOP_OFFER, item) } })
//        trans?.addToBackStack(TopOfferDetailFragment::class.java.simpleName)
//        trans?.commitAllowingStateLoss()
        openActivity<BuySellDetailActivity> {  }
    }
}