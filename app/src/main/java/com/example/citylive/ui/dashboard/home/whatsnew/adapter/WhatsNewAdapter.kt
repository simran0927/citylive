package com.example.citylive.ui.dashboard.home.whatsnew.adapter





import com.example.citylive.base.BaseListAdapter
import com.example.citylive.databinding.ItemWhatsNewBinding
import com.example.citylive.util.loadImage


class WhatsNewAdapter :
    BaseListAdapter<Int, ItemWhatsNewBinding>(ItemWhatsNewBinding::class.java) {
    override fun onBindViewHolder(holder: IViewHolder<ItemWhatsNewBinding>, position: Int) {
        val data = getItem(position)
        holder.binding.imgTopPick.loadImage(data)

    }

}