package com.example.citylive.ui

import android.os.Handler
import android.os.Looper
import com.example.citylive.R
import com.example.citylive.base.BaseActivity
import com.example.citylive.databinding.ActivitySplashBinding
import com.example.citylive.ui.auth.LoginActivity
import com.example.citylive.util.makeStatusBarTransparent
import com.example.citylive.util.openActivity

class SplashActivity : BaseActivity<ActivitySplashBinding>(ActivitySplashBinding::class.java) {
    override fun setViews() {
        makeStatusBarTransparent(R.id.rootSplash)

//
//        Handler().postDelayed({
//            openActivity<LoginActivity>()
//            finish()
//        }, 1500)
        Handler(Looper.getMainLooper()).postDelayed({
            openActivity<LoginActivity>()


            Handler(Looper.getMainLooper()).postDelayed({
                finish()
            }, 1000)
        }, 1500)
    }

}
