package com.example.citylive.ui.dashboard.home.adapter

import com.example.citylive.base.BaseListAdapter
import com.example.citylive.databinding.ItemTopPickBinding


class HomeTopPickAdapter :
    BaseListAdapter<Int, ItemTopPickBinding>(ItemTopPickBinding::class.java) {
    override fun onBindViewHolder(holder: IViewHolder<ItemTopPickBinding>, position: Int) {
        val data = getItem(position)
        holder.binding.imgTopPick.setImageResource(data)

    }

}