package com.example.citylive.ui.dashboard.home.directions

import com.example.citylive.R
import com.example.citylive.base.BaseFragment
import com.example.citylive.databinding.FragmentDirectionsBinding
import com.example.citylive.ui.dashboard.home.adapter.HomeCat
import com.example.citylive.ui.dashboard.home.directions.adapter.Directions
import com.example.citylive.ui.dashboard.home.directions.adapter.DirectionsAdapter

class DirectionsFragment : BaseFragment<FragmentDirectionsBinding>(FragmentDirectionsBinding::class.java) {

    private val topPickAdapter by lazy { DirectionsAdapter() }
    override fun setViews() {


        binding.listTopPick.adapter = topPickAdapter
        topPickAdapter.submitList(arrayListOf(Directions("Mobile Store Radhe Communication","9375511851"),
            Directions("Grocerry Store Jiaan Super Market","8401103703"),
            Directions("Ice Cream Shop Patel Parlour","8487923322"),
            Directions("Jewellery Store RadheShyam Jewellers","9898583177"),
            Directions("Sweets & Snacks Bajarang Sweets","9898091101"),
            Directions("Ready-made Clothing Store ShreeJi Selection","7043242444"),
            Directions("Medical Store Gaytri Medical","7878177178"),
            Directions("Insurance Provider RudraOnline Services","8238499197"),
            Directions("Restaurants Panjab Da Tadka","9737732440"),
            Directions("Cinema House Silver Screen","9898710212"),
            Directions("Interior Designer Kalasagar Glasses","9998153580"),
            Directions("Broadband Provider GTPL-Jio","9016313100"),
            Directions("Website & Mobile App Developer CityLive Network","9870039989"),
            Directions("Print Shop DholkaLive","9870039989"),
            Directions("Graphics Designer","9870039989"),
            Directions("Digital Marketing Agency","9870039989"),
            Directions("Property Delears","9870039989"),

            ))
    }
}