package com.example.citylive.ui.dashboard.home.topoffer.adapter

import android.os.Parcel
import android.os.Parcelable
import com.example.citylive.base.BaseListAdapter
import com.example.citylive.databinding.ItemTopPickBinding
import com.example.citylive.util.loadImage
import kotlinx.android.parcel.Parcelize

@Parcelize
data class HomeTopPick(
    val image: Int,
    val offerName: String,
    val phoneNumber: String,
    val wnumber: String
): Parcelable

class HomeTopPickAdapter(val clickListener: (HomeTopPick) -> Unit) :
    BaseListAdapter<HomeTopPick, ItemTopPickBinding>(ItemTopPickBinding::class.java) {
    override fun onBindViewHolder(holder: IViewHolder<ItemTopPickBinding>, position: Int) {
        val data = getItem(position)
        holder.binding.imgTopPick.loadImage(data.image)
        holder.itemView.setOnClickListener {
            clickListener(getItem(holder.adapterPosition))
        }
    }


}