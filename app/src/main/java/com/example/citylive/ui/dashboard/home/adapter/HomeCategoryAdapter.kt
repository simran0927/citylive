package com.example.citylive.ui.dashboard.home.adapter

import com.example.citylive.base.BaseListAdapter
import com.example.citylive.databinding.ItemHomeCatBinding
import com.example.citylive.util.loadImage

data class HomeCat(val image: Int, val title: String)

class HomeCategoryAdapter(val onClick: (HomeCat) -> Unit): BaseListAdapter<HomeCat, ItemHomeCatBinding>(ItemHomeCatBinding::class.java) {
    override fun onBindViewHolder(holder: IViewHolder<ItemHomeCatBinding>, position: Int) {
        val data = getItem(position)
        holder.binding.catImage.loadImage(data.image)
        holder.binding.catTitle.text = data.title

        holder.itemView.setOnClickListener {
            onClick(getItem(holder.adapterPosition))
        }
    }
}