package com.example.citylive.ui.dashboard.home.topoffer

import TopOfferFragment
import android.widget.Toast
import com.example.citylive.R
import com.example.citylive.base.BaseActivity
import com.example.citylive.base.ViewPagerAdapter
import com.example.citylive.databinding.ActivityTopOfferDetailBinding
import com.example.citylive.util.loadImage

class TopOfferDetailActivity : BaseActivity<ActivityTopOfferDetailBinding>(ActivityTopOfferDetailBinding::class.java) {


    override fun setViews() {

        binding.offerImage.loadImage(R.drawable.top_offers)
        binding.contactPager.adapter= supportFragmentManager?.let { ViewPagerAdapter(it,
            arrayListOf(OfferTabItemFragment(),OfferTabItemTwoFragmentFragment()), arrayListOf("Mobile Number", "Whatsapp Number"),true) }
        binding.tabLayout.setupWithViewPager(binding.contactPager)


    }
}