package com.example.citylive.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.citylive.R
import com.example.citylive.base.BaseActivity
import com.example.citylive.databinding.ActivityHomeBinding

class HomeActivity : BaseActivity<ActivityHomeBinding>(ActivityHomeBinding::class.java) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
    }

    override fun setViews() {
      setSupportActionBar(binding.homeToolbar)
        supportActionBar?.setTitle("Home")
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

    }
}