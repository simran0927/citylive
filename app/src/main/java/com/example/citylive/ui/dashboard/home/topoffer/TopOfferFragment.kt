import com.example.citylive.R
import com.example.citylive.base.BaseFragment
import com.example.citylive.databinding.FragmentTopOfferBinding
import com.example.citylive.ui.dashboard.home.topoffer.TopOfferDetailActivity
import com.example.citylive.ui.dashboard.home.topoffer.adapter.HomeTopPick
import com.example.citylive.ui.dashboard.home.topoffer.adapter.HomeTopPickAdapter
import com.example.citylive.util.openActivity


class TopOfferFragment : BaseFragment<FragmentTopOfferBinding>(FragmentTopOfferBinding::class.java) {

    private val topPickAdapter by lazy { HomeTopPickAdapter(this::onItemClick) }
    override fun setViews() {

        binding.listTopPick.adapter = topPickAdapter

        topPickAdapter.submitList(arrayListOf(
            HomeTopPick(R.drawable.top_offers, "Offer One","1234567890","1234567890"),
            HomeTopPick(R.drawable.top_offers2, "Offer Two","1234567890","1234567890"),
            HomeTopPick(R.drawable.top_offers3, "Offer Three","1234567890","1234567890"),

            HomeTopPick(R.drawable.top_offers4, "Offer Four","1234567890","1234567890")
        ))
    }


    private fun onItemClick(item: HomeTopPick) {
//        val trans = activity?.supportFragmentManager?.beginTransaction()
//        trans?.add(R.id.dashboardContainer, TopOfferDetailFragment().apply { Bundle().apply { putParcelable(Intentkey.HOME_TOP_OFFER, item) } })
//        trans?.addToBackStack(TopOfferDetailFragment::class.java.simpleName)
//        trans?.commitAllowingStateLoss()
        openActivity<TopOfferDetailActivity> {  }
    }


}