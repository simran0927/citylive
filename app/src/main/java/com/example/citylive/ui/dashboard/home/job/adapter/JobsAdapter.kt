package com.example.citylive.ui.dashboard.home.job.adapter



import android.os.Parcelable
import com.bumptech.glide.Glide
import com.example.citylive.R
import com.example.citylive.base.BaseListAdapter
import com.example.citylive.databinding.ItemJobsBinding
import com.example.citylive.ui.dashboard.home.topoffer.adapter.HomeTopPick
import com.example.citylive.util.loadImage
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ImagePick(
    val image: Int,

):Parcelable


class JobsAdapter(val clickListener: () -> Unit) :
    BaseListAdapter<ImagePick, ItemJobsBinding>(ItemJobsBinding::class.java) {
    override fun onBindViewHolder(holder: IViewHolder<ItemJobsBinding>, position: Int) {
        val data = getItem(position)
       // Glide.with(holder.itemView.context).load(data.image).into(holder.binding.imgJobPick)
        holder.binding.imgTopPick.loadImage(data.image)
 holder.itemView.setOnClickListener{
     clickListener()
 }
    }

}