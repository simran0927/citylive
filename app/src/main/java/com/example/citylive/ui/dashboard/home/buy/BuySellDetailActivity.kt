package com.example.citylive.ui.dashboard.home.buy

import TopOfferFragment
import android.widget.Toast
import com.example.citylive.R
import com.example.citylive.base.BaseActivity
import com.example.citylive.base.ViewPagerAdapter
import com.example.citylive.databinding.ActivityBuySellDetailBinding
import com.example.citylive.databinding.ActivityJobDetailBinding
import com.example.citylive.databinding.ActivityTopOfferDetailBinding
import com.example.citylive.ui.dashboard.home.topoffer.OfferEmailTabFragment
import com.example.citylive.ui.dashboard.home.topoffer.OfferTabItemFragment
import com.example.citylive.ui.dashboard.home.topoffer.OfferTabItemTwoFragmentFragment
import com.example.citylive.util.loadImage

class BuySellDetailActivity : BaseActivity<ActivityBuySellDetailBinding>(ActivityBuySellDetailBinding::class.java) {


    override fun setViews() {
        binding.offerImage.loadImage(R.drawable.buy)
        binding.contactPager.adapter= supportFragmentManager?.let { ViewPagerAdapter(it,
            arrayListOf(OfferTabItemFragment(), OfferTabItemTwoFragmentFragment()), arrayListOf("Mobile Number", "Whatsapp Number"),true) }
        binding.tabLayout.setupWithViewPager(binding.contactPager)


    }
}