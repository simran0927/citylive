package com.example.citylive.ui.dashboard.home.job

import TopOfferFragment
import android.widget.Toast
import com.example.citylive.R
import com.example.citylive.base.BaseActivity
import com.example.citylive.base.ViewPagerAdapter
import com.example.citylive.databinding.ActivityJobDetailBinding
import com.example.citylive.databinding.ActivityTopOfferDetailBinding
import com.example.citylive.ui.dashboard.home.topoffer.OfferEmailTabFragment
import com.example.citylive.ui.dashboard.home.topoffer.OfferTabItemFragment
import com.example.citylive.ui.dashboard.home.topoffer.OfferTabItemTwoFragmentFragment
import com.example.citylive.util.loadImage

class JobDetailActivity : BaseActivity<ActivityJobDetailBinding>(ActivityJobDetailBinding::class.java) {


    override fun setViews() {
        binding.offerImage.loadImage(R.drawable.job)
        binding.contactPager.adapter= supportFragmentManager?.let { ViewPagerAdapter(it,
                arrayListOf(OfferTabItemFragment(), OfferEmailTabFragment()), arrayListOf("Mobile Number", "EmailId"),true) }
        binding.tabLayout.setupWithViewPager(binding.contactPager)


    }
}