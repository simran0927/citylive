package com.example.citylive.ui.dashboard.home.directions.adapter







import com.example.citylive.base.BaseListAdapter
import com.example.citylive.databinding.ItemDirectionsBinding


data class Directions(val name: String, val number: String)
class DirectionsAdapter :
    BaseListAdapter<Directions, ItemDirectionsBinding>(ItemDirectionsBinding::class.java) {
    override fun onBindViewHolder(holder: IViewHolder<ItemDirectionsBinding>, position: Int) {
        val data = getItem(position)
    holder.binding.dName.text= data.name
        holder.binding.dNumber.text=data.number

    }

}