package com.example.citylive.ui.dashboard.home.whatsnew

import com.example.citylive.R
import com.example.citylive.base.BaseFragment
import com.example.citylive.databinding.FragmentWhatsNewBinding
import com.example.citylive.ui.dashboard.home.whatsnew.adapter.WhatsNewAdapter

class WhatsNewFragment : BaseFragment<FragmentWhatsNewBinding>(FragmentWhatsNewBinding::class.java) {

    private val topPickAdapter by lazy { WhatsNewAdapter() }
    override fun setViews() {


        binding.listTopPick.adapter = topPickAdapter
        topPickAdapter.submitList(arrayListOf(R.drawable.whatsnew, R.drawable.whatnew2, R.drawable.whatsnew3, R.drawable.whatsnew4))
    }
}