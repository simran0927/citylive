package com.example.citylive.ui.dashboard.home.job.adapter



import com.example.citylive.base.BaseListAdapter
import com.example.citylive.databinding.ItemBuyBinding
import com.example.citylive.util.loadImage


class BuyAdapter(val clickListener: () -> Unit) :
    BaseListAdapter<Int, ItemBuyBinding>(ItemBuyBinding::class.java) {
    override fun onBindViewHolder(holder: IViewHolder<ItemBuyBinding>, position: Int) {
        val data = getItem(position)
        holder.binding.imgTopPick.loadImage(data)
        holder.itemView.setOnClickListener{
            clickListener()
        }

    }

}