package com.example.citylive.ui.dashboard

import android.view.Menu
import android.view.View
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.fragment.app.Fragment
import com.example.citylive.R
import com.example.citylive.base.BaseActivity
import com.example.citylive.databinding.ActivityDashboardBinding
import com.example.citylive.ui.dashboard.aboutUs.AboutUsFragment
import com.example.citylive.ui.dashboard.contact.ContactUsFragment
import com.example.citylive.ui.dashboard.home.HomeFragment


class DashboardActivity :
    BaseActivity<ActivityDashboardBinding>(ActivityDashboardBinding::class.java) {

    override fun setViews() {
       // setToolbar()
        setBottomNavigation()
        moveToFragment(HomeFragment())
    }

//    private fun setToolbar() {
//        setSupportActionBar(binding.toolbar)
//        supportActionBar?.setDisplayShowHomeEnabled(true)
//        supportActionBar?.setHomeButtonEnabled(true)
//        supportActionBar?.setDisplayShowTitleEnabled(false)
//        val toggle = ActionBarDrawerToggle(
//            this,
//            binding.drawerLayout,
//            binding.toolbar,
//            R.string.navigation_drawer_open,
//            R.string.navigation_drawer_close
//        )
//        toggle.isDrawerIndicatorEnabled = true
//        binding.drawerLayout.addDrawerListener(toggle)
//        toggle.syncState()
//    }

    private fun setBottomNavigation() {
        binding.bottomNavigation.itemIconTintList = null
        binding.bottomNavigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.home -> {
                    binding.toolbar.visibility=View.VISIBLE
                    moveToFragment(HomeFragment())
                }
                R.id.gold -> {
                    binding.toolbar.visibility=View.GONE
                    moveToFragment(ContactUsFragment())

                }

                R.id.story -> {
                    binding.toolbar.visibility=View.GONE
                    moveToFragment(AboutUsFragment())

                }

                else ->{
                    binding.toolbar.visibility=View.VISIBLE
                    moveToFragment(HomeFragment())}
            }
            true
        }
    }

    private fun moveToFragment(fragment: Fragment) {
        val trans = supportFragmentManager.beginTransaction()
        trans.add(R.id.dashboardContainer, fragment)
        trans.addToBackStack(null)
        trans.commitAllowingStateLoss()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        return true
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1) {
            finish()
        } else {
            super.onBackPressed()
        }
    }


}