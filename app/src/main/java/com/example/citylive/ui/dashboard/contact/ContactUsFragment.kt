package com.example.citylive.ui.dashboard.contact



import android.annotation.SuppressLint
import android.os.Build
import android.text.Html
import android.text.method.LinkMovementMethod
import android.widget.TextView
import com.example.citylive.base.BaseFragment
import com.example.citylive.databinding.FragmentContactUsBinding
import com.example.citylive.util.openWebPageIntent


class ContactUsFragment : BaseFragment<FragmentContactUsBinding>(FragmentContactUsBinding::class.java) {


    override fun setViews() {
        binding.tagInfo.setOnClickListener {
            openWebPageIntent("https://www.einfosoft.in/")
        }



    }



}