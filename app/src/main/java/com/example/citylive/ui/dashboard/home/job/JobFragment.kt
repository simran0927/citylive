import com.example.citylive.R
import com.example.citylive.base.BaseFragment
import com.example.citylive.databinding.FragmentJobBinding
import com.example.citylive.ui.dashboard.home.job.JobDetailActivity
import com.example.citylive.ui.dashboard.home.job.adapter.ImagePick
import com.example.citylive.ui.dashboard.home.job.adapter.JobsAdapter
import com.example.citylive.ui.dashboard.home.topoffer.TopOfferDetailActivity
import com.example.citylive.ui.dashboard.home.topoffer.adapter.HomeTopPick
import com.example.citylive.util.openActivity

class JobFragment : BaseFragment<FragmentJobBinding>(FragmentJobBinding::class.java) {

    private val topPickAdapter by lazy { JobsAdapter(this::onItemClick) }
    override fun setViews() {


        binding.listTopPick.adapter = topPickAdapter
        topPickAdapter.submitList(arrayListOf(ImagePick(R.drawable.job), ImagePick(R.drawable.job2)))
    }


    private fun onItemClick() {
//        val trans = activity?.supportFragmentManager?.beginTransaction()
//        trans?.add(R.id.dashboardContainer, TopOfferDetailFragment().apply { Bundle().apply { putParcelable(Intentkey.HOME_TOP_OFFER, item) } })
//        trans?.addToBackStack(TopOfferDetailFragment::class.java.simpleName)
//        trans?.commitAllowingStateLoss()
        openActivity<JobDetailActivity> {  }
    }
}