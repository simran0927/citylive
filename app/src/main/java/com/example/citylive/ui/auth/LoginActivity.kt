package com.example.citylive.ui.auth

import android.os.Handler
import android.os.Looper
import com.example.citylive.R
import com.example.citylive.base.BaseActivity
import com.example.citylive.databinding.ActivityLoginBinding
import com.example.citylive.ui.dashboard.DashboardActivity
import com.example.citylive.util.makeStatusBarTransparent
import com.example.citylive.util.openActivity

class LoginActivity : BaseActivity<ActivityLoginBinding>(ActivityLoginBinding::class.java) {

    override fun setViews() {
        binding.btnLogin.setOnClickListener {
            openActivity<DashboardActivity>()
            Handler(Looper.getMainLooper()).postDelayed({
                finish()
            }, 1000)
        }
    }

}