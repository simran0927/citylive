package com.example.citylive.ui.dashboard.home

import JobFragment
import TopOfferFragment
import androidx.fragment.app.Fragment
import com.example.citylive.R
import com.example.citylive.base.BaseFragment
import com.example.citylive.databinding.FragmentHomeBinding
import com.example.citylive.ui.dashboard.home.adapter.HomeCat
import com.example.citylive.ui.dashboard.home.adapter.HomeCategoryAdapter
import com.example.citylive.ui.dashboard.home.buy.BuySellFragment
import com.example.citylive.ui.dashboard.home.directions.DirectionsFragment
import com.example.citylive.ui.dashboard.home.whatsnew.WhatsNewFragment
import com.example.citylive.util.loadImage
import com.example.citylive.widget.ItemOffsetDecoration

class HomeFragment : BaseFragment<FragmentHomeBinding>(FragmentHomeBinding::class.java) {

    private val catAdapter by lazy { HomeCategoryAdapter(this::onCategoryClick) }
    override fun setViews() {
        binding.listCategory.addItemDecoration(ItemOffsetDecoration(requireContext(), 1f, 1f))
        binding.listCategory.adapter = catAdapter
        catAdapter.submitList(
            arrayListOf(
                HomeCat(R.drawable.offer, "Top Offers"),
                HomeCat(R.drawable.ic_job, "Job"),
                HomeCat(R.drawable.trade, "Buy-sell-rent"),
                HomeCat(R.drawable.directions, "Utility Directory"),
                HomeCat(R.drawable.ic_new, "WhatsNew")


            )
        )
        binding.bannerFestival.loadImage(R.drawable.top_banner)
        binding.bannerOffer.loadImage(R.drawable.dasboard_image)
        moveToFragment(TopOfferFragment())
    }

    private fun moveToFragment(fragment: Fragment) {
        val trans = childFragmentManager.beginTransaction()
        trans.replace(R.id.dashboard, fragment)
        trans.addToBackStack(null)
        trans.commitAllowingStateLoss()
    }

    private fun onCategoryClick(cat: HomeCat) {
        when (cat.title) {
            "Top Offers" -> {
                moveToFragment(TopOfferFragment())
            }
            "Job" -> {
                moveToFragment(JobFragment())
            }
            "Buy-sell-rent" -> {
                moveToFragment(BuySellFragment())
            }
            "WhatsNew" -> {
                moveToFragment(WhatsNewFragment())
            }
            "Utility Directory" -> {
                moveToFragment(DirectionsFragment())
            }
        }
    }
}