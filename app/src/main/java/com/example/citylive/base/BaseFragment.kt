package com.example.citylive.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding

abstract class BaseFragment< BINDING : ViewBinding>(private val viewBinding: Class<BINDING>) : Fragment() {

    lateinit var binding: BINDING

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        initBinding(inflater, container)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setViews()
    }

    @Suppress("UNCHECKED_CAST")
    fun initBinding(inflater: LayoutInflater, container: ViewGroup?) {
        val inflaterMethod = viewBinding.getDeclaredMethod(
            "inflate",
            LayoutInflater::class.java,
            ViewGroup::class.java,
            Boolean::class.java
        )
         binding = inflaterMethod.invoke(null, inflater, container, false) as BINDING
    }

    abstract fun setViews()
}