package com.example.citylive.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding

/***
Base class for ListAdapter

class CommentListAdapter: BaseListAdapter<String, ItemCommentBinding>(ItemCommentBinding::class.java)

class CommentListAdapter :
BaseListAdapter<String, ItemCommentBinding>(ItemCommentBinding::class.java,
areSame = { s, s2 -> false },
areCSame = { s, s2 -> true })

***/

abstract class BaseListAdapter<ITEM, BINDING : ViewBinding>(
    private val viewBinding: Class<BINDING>,
    areSame: (ITEM, ITEM) -> Boolean = { item, item2 -> item == item2 },
    areCSame: (ITEM, ITEM) -> Boolean = { item, item2 -> item == item2 }
) :
    ListAdapter<ITEM, BaseListAdapter.IViewHolder<BINDING>>(
       ListItemCallback<ITEM>(
            areSame,
            areCSame
        )
    ) {


    class IViewHolder<BINDING>(val binding: BINDING) :
        RecyclerView.ViewHolder((binding as ViewBinding).root)

    @Suppress("UNCHECKED_CAST")
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IViewHolder<BINDING> {
        val inflaterMethod = viewBinding.getDeclaredMethod(
            "inflate",
            LayoutInflater::class.java,
            ViewGroup::class.java,
            Boolean::class.java
        )
        val binding =
            inflaterMethod.invoke(null, LayoutInflater.from(parent.context), parent, false)
        return IViewHolder(binding = binding as BINDING)
    }

    abstract override fun onBindViewHolder(holder: IViewHolder<BINDING>, position: Int)

    class ListItemCallback<ITEM>(
        val areSame: (ITEM, ITEM) -> Boolean,
        val areCSame: (ITEM, ITEM) -> Boolean
    ) : DiffUtil.ItemCallback<ITEM>() {
        override fun areItemsTheSame(oldItem: ITEM, newItem: ITEM): Boolean {
            return areSame(oldItem, newItem)
        }

        override fun areContentsTheSame(oldItem: ITEM, newItem: ITEM): Boolean {
            return areCSame(oldItem, newItem)
        }

    }
}