package com.example.citylive.base

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.viewbinding.ViewBinding

abstract class BaseActivity< BINDING : ViewBinding>(private val viewBinding: Class<BINDING>) : AppCompatActivity() {

    lateinit var binding: BINDING
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initBinding()
        setContentView(binding.root)
        setViews()
    }

    @Suppress("UNCHECKED_CAST")
    fun initBinding() {
        val inflaterMethod = viewBinding.getDeclaredMethod(
            "inflate",
            LayoutInflater::class.java,
            ViewGroup::class.java,
            Boolean::class.java
        )
         binding = inflaterMethod.invoke(null, LayoutInflater.from(this), parent, false) as BINDING
    }

    abstract fun setViews()
}