package com.example.citylive.util

import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.view.ViewGroup
import android.widget.ImageView
import androidx.annotation.DrawableRes
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.example.citylive.R


fun <T> ImageView.loadImage(
    source: T,
    @DrawableRes errorImage: Int = 0
) {
    Glide.with(this.context)
        .load(source)
        .apply(requestOptions(errorImage))
//        .transition(DrawableTransitionOptions.withCrossFade())
        .into(this)

}

fun ImageView.requestOptions(errorImage: Int): RequestOptions {
    return RequestOptions()
        .useUnlimitedSourceGeneratorsPool(true)
        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
        .centerCrop()
        .placeholder(GradientDrawable().apply { setColor(context.getColorCompat(R.color.colorPrimary)) })
        .error(errorImage)
        .override(width, height)
}

/**
 * Setting Image to a background
 *
 * @param imageURL: The URL needed to be Displayed
 */
fun ViewGroup.loadImageAsBackground(imageURL: String) {
    Glide.with(context)
        .load(imageURL)
        .diskCacheStrategy(DiskCacheStrategy.DATA)
        .into(object :
            CustomTarget<Drawable>() {
            override fun onLoadCleared(placeholder: Drawable?) {

            }

            override fun onResourceReady(
                resource: Drawable,
                transition: Transition<in Drawable>?
            ) {
                background = resource
            }

        })
}

